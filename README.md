### Universis Api Server Plugin Example

This project contains an example plugin of [universis-api](https://gitlab.com/universis/universis-api) server.

#### Description

Provides a set of data models for managing locations, places, buildings, classrooms etc. It acts as an application service which
registers each model that does not exist in parent api server.

    // modules/example/index.js
    ...
    // load models
    const files = fs.readdirSync(path.resolve(__dirname, 'config/models'));
    files.forEach( file => {
        // load model definition
        const model = require(path.resolve(__dirname, 'config/models', file));
        // try to get model from application (if model definition does not exists in parent configuration)
        if (configuration.model(model.name) == null) {
            // set model definition
            configuration.setModelDefinition(model);
        }
    });
    ...

#### Installation

Clone repository

    git clone https://gitlab.com/universis/universis-api-plugin-example
    
Install dependencies:

    npm i

Build project:

    npm run build

Navigate to dist folder:

    cd dist/example    

Link module:

    npm link
    
Go to your universis api server root and execute:

    npm link universis-api-plugin-example
    
Open universis api server configuration and add ExamplePlugin as service:

    {
        "services": [
            {
                "serviceType": "universis-api-plugin-example#ExamplePlugin"
            },
            {
                "serviceType": "./services/oauth2-client-service#OAuth2ClientService"
            }
            ...
        ]
        ...
    }

Finally start you api server 
and use http://localhost:5001/api/places/ endpoints to view and manage places.



 
